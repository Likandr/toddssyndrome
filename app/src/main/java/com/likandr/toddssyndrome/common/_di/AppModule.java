package com.likandr.toddssyndrome.common._di;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.likandr.toddssyndrome.common.App;

import dagger.Module;
import dagger.Provides;

@Module
public final class AppModule {
    private App app;

    public AppModule(App app) {
        this.app = app;
    }

    @Provides @ApplicationScope
    public App provideApplication() {
        return app;
    }

    @Provides @ApplicationScope
    public Context provideApplicationContext() {
        return app.getApplicationContext();
    }

    @Provides @ApplicationScope
    public SharedPreferences provideSharedPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }
}