package com.likandr.toddssyndrome.common._di;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.likandr.toddssyndrome.api.NetModule;
import com.likandr.toddssyndrome.common.App;

import dagger.Component;

@ApplicationScope
@Component(
        modules = {AppModule.class, NetModule.class}
)
public interface AppComponent {

    void inject(App app);

    Context context();
    App app();
    Gson gson();
    SharedPreferences sharedPreferences();
}