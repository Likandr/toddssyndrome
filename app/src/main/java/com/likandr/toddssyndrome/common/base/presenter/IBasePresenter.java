package com.likandr.toddssyndrome.common.base.presenter;

import com.likandr.toddssyndrome.common.base.BaseMvpView;

public interface IBasePresenter<V extends BaseMvpView> {

    void attachView(V view);
    void detachView();
    boolean isViewAttached();
    void checkViewAttached();
}