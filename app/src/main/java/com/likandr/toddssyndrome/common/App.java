package com.likandr.toddssyndrome.common;

import android.app.Application;

import com.likandr.toddssyndrome.BuildConfig;
import com.likandr.toddssyndrome.common._di.AppComponent;
import com.likandr.toddssyndrome.common._di.AppModule;
import com.likandr.toddssyndrome.common._di.DaggerAppComponent;
import com.squareup.leakcanary.LeakCanary;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class App extends Application {

    public static final String TAG = App.class.getSimpleName();

    private static App sInstance;
    private static RealmConfiguration realmConfiguration;
    private AppComponent component;


    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;

        if (BuildConfig.DEBUG) {
            LeakCanary.install(this);
        }

        this.setUpRealmConfig();
        this.setAppComponent();
    }

    public AppComponent getComponent() {
        return component;
    }

    private void setUpRealmConfig() {
        Realm.init(getApplicationContext());
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder().
                deleteRealmIfMigrationNeeded().
                build();
        Realm.setDefaultConfiguration(realmConfiguration);
    }

    private void setAppComponent() {
        component = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    public static App get() {
        return sInstance;
    }
}