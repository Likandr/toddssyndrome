package com.likandr.toddssyndrome.common.misc;

import android.util.Log;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class Utils {

    private static final String TAG = "common.misc.Utils";

    private static Utils instance = new Utils();

    public static Utils getInstance() {
        return instance;
    }

    public static void setInstance(Utils instance) {
        Utils.instance = instance;
    }

    public Integer serverErrorCode(JsonElement response) {
        JsonObject json = response.getAsJsonObject();
        int errorCode = json.get("errorCode").getAsInt();
        if (errorCode != 0) {
            String message = json.get("errorMessage").getAsString();
            Log.e(TAG, "Server returned " + errorCode + " code with message: " + message);
        }
        return errorCode;
    }
}
