package com.likandr.toddssyndrome.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.realm.annotations.PrimaryKey;

public class QuestionData {

    @SerializedName("result")
    private List<Question> questions;

    public QuestionData() {
    }

    public QuestionData(List<Question> questions) {
        this.questions = questions;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }
}
