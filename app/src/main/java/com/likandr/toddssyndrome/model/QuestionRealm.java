package com.likandr.toddssyndrome.model;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class QuestionRealm extends RealmObject implements Copyable<QuestionRealm> {

    @PrimaryKey
    @SerializedName("question_id")
    private String id;

    @SerializedName("question_text")
    private String text;

    @SerializedName("weight")
    private double weight;

    public QuestionRealm() {
    }

    public QuestionRealm(String id, String text, double weight) {
        this.id = id;
        this.text = text;
        this.weight = weight;
    }

    @Override
    public QuestionRealm getCopy() {
        return new QuestionRealm(getId(), getText(), getWeight());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
}