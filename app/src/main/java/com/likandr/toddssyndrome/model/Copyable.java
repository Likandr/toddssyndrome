package com.likandr.toddssyndrome.model;


public interface Copyable<T extends Copyable<T>> {
    T getCopy();
}