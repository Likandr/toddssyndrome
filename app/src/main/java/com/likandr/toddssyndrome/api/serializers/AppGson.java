package com.likandr.toddssyndrome.api.serializers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.likandr.toddssyndrome.model.QuestionRealm;

import java.lang.reflect.Type;
import java.util.List;

public class AppGson {
    private static GsonBuilder baseBuilder = new GsonBuilder().setDateFormat("dd.MM.yyyy");
    private static Gson base = baseBuilder.create();
    private static Gson instance;

    public static GsonBuilder getBaseBuilder() {
        return baseBuilder;
    }

    public static Gson getBase() {
        return base;
    }

    public static Gson getInstance() {
        if (instance == null) {
            Type questionType = new TypeToken<List<QuestionRealm>>(){}.getType();
            instance = baseBuilder
                    .registerTypeAdapter(questionType, new GetQuestionDeserializer())
                    .create();
        }
        return instance;
    }
}
