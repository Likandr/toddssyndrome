package com.likandr.toddssyndrome.api.mock;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

public class SeedManager {

    private static final String TAG = "Seedmanager";

    private String basePath = "/api";

    public SeedManager() { }

    public SeedManager(String basePath) {
        this.basePath = basePath;
    }

    public String getJson(String path) {
        String fileName = getFullPath(path);
        URL url = this.getClass().getResource(fileName);
        StringBuilder builder = null;
        try {
            InputStream stream = url.openStream();
            InputStreamReader streamReader = new InputStreamReader(stream, "UTF-8");
            BufferedReader reader = new BufferedReader(streamReader);
            builder = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
        } catch (IOException e) {
            Log.e(TAG, "Error reading seed file: " + e.getMessage());
            System.exit(1);
        }
        return builder.toString();
    }

    protected String getFullPath(String path) {
        return basePath + "/" + path + ".json";
    }
}
