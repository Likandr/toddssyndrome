package com.likandr.toddssyndrome.api.serializers;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.likandr.toddssyndrome.model.QuestionRealm;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class GetQuestionDeserializer extends Deserializer<List<QuestionRealm>> {
    @Override
    public List<QuestionRealm> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        super.deserialize(json, typeOfT, context);
        List<QuestionRealm> result = new ArrayList<>();
        JsonArray questionArray = json
                .getAsJsonObject()
                .getAsJsonArray("result");
        for (JsonElement question : questionArray) {
            QuestionRealm questionRealm = AppGson.getBase().fromJson(question, QuestionRealm.class);
            result.add(questionRealm);
        }
        return result;
    }
}
