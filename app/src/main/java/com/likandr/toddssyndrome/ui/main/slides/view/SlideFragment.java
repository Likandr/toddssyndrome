package com.likandr.toddssyndrome.ui.main.slides.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.cremy.greenrobotutils.library.ui.SnackBarUtils;
import com.likandr.toddssyndrome.R;
import com.likandr.toddssyndrome.common.App;
import com.likandr.toddssyndrome.common.base.BaseActivity;
import com.likandr.toddssyndrome.common.base.BaseFragment;
import com.likandr.toddssyndrome.common.base.Layout;
import com.likandr.toddssyndrome.model.Entry;
import com.likandr.toddssyndrome.ui.main._di.DaggerMainComponent;
import com.likandr.toddssyndrome.ui.main._di.MainComponent;
import com.likandr.toddssyndrome.ui.main._di.MainModule;
import com.likandr.toddssyndrome.ui.main.slides.SlideMVP;
import com.likandr.toddssyndrome.ui.main.slides.SlidePresenter;

import javax.inject.Inject;

import butterknife.BindView;

@Layout(id = R.layout.fragment_slide)
public class SlideFragment extends BaseFragment implements SlideMVP.View,
        Toolbar.OnMenuItemClickListener {

    public static final String TAG = SlideFragment.class.getSimpleName();

    @BindView(R.id.root_view)
    CoordinatorLayout rootView;
    @BindView(R.id.pager)
    ViewPager pager;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    MainComponent component;
    @Inject SlidePresenter presenter;

    SlidePagerAdapter pagerAdapter;

    @Override
    public void injectDependencies() {
        Log.d(TAG, "injectDependencies");
        if (component == null) {
            component = DaggerMainComponent.builder()
                    .appComponent(App.get().getComponent())
                    .mainModule(new MainModule(this))
                    .build();
            component.inject(this);
        }
    }

    @Override
    public void attachToPresenter() {
        Log.d(TAG, "attachToPresenter");
        this.presenter.attachView(this);
    }

    @Override
    public void detachFromPresenter() {
        Log.d(TAG, "detachFromPresenter");
        this.presenter.detachView();
    }

    public SlideFragment() {
        // Required empty public constructor
    }

    public static SlideFragment newInstance() {
        return new SlideFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        this.getArgs(savedInstanceState);
        this.setUpToolbar();
    }

    @Override
    public void setUpToolbar() {
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        BaseActivity activity = (BaseActivity) getActivity();
        activity.setSupportActionBar(toolbar);

        toolbar.setOnMenuItemClickListener(this);

        ActionBar actionBar = activity.getSupportActionBar();
        if(actionBar!= null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(v -> activity.onBackPressed());
            toolbar.setTitle("Questions");
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
        super.onCreateOptionsMenu(menu, inflater);

        onSetupMenu(menu);
    }

    private void onSetupMenu(Menu menu) {
        MenuItem exitMenuItem = menu.findItem(R.id.action_clear_history);
        exitMenuItem.setVisible(false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onViewCreated");
        super.onViewCreated(view, savedInstanceState);
        setupPager();
        presenter.start();
    }

    private void setupPager() {
        Log.d(TAG, "setupPager");
        pagerAdapter = new SlidePagerAdapter(getFragment(), getChildFragmentManager());
        presenter.setAdapter(pagerAdapter);

        pager.setAdapter(pagerAdapter);
        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                Log.d(TAG, "onPageSelected, position = " + position);
                //when last page | The text is updated and the result is output
                if (position == pagerAdapter.getCount() - 1) pagerAdapter.notifyDataSetChanged();
            }

            @Override
            public void onPageScrolled(int position,
                                       float positionOffset,
                                       int positionOffsetPixels) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        //block swipes | jump to next page using buttons "yes" and "no"
        pager.setOnTouchListener((arg0, arg1) -> true);
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }

    //----
    @Override
    public void onAttach(Context context) {
        Log.d(TAG, "onAttach");
        this.injectDependencies();
        this.attachToPresenter();
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        Log.d(TAG, "onDetach");
        detachFromPresenter();
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        Log.d(TAG, "onDestroyView");
        pagerAdapter.onDestroy();
        presenter.closeRealm();
        super.onDestroyView();
    }

    @Override
    public void getArgs(Bundle _bundle) {
        // Empty intentionally.
    }

    /*@Override
    public void onLandscape() {
        // Empty intentionally.
    }
    @Override
    public void onPortrait() {
        // Empty intentionally.
    }*/

    @Override
    public void showLoading() {
        Log.d(TAG, "showLoading");
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        Log.d(TAG, "hideLoading");
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showMessage(String message) {
        Log.d(TAG, "showMessage");
        SnackBarUtils.showSimpleSnackbar(rootView, message);
    }

    /*@Override
    public void showNoNetwork() {
       // Empty intentionally.
    }*/

    public void goNextPage() {
        Log.d(TAG, "goNextPage");
        pager.setCurrentItem(pager.getCurrentItem() + 1);
    }

    @Override
    public void saveNewEntry(@NonNull Entry entry) {
        Log.d(TAG, "saveNewEntry");
        presenter.addEntry(entry);
    }

    public interface Updateable {
        void update(String str);
    }

    /*public void backToHistory() {
        FragmentManager manager = getActivity().getSupportFragmentManager();
        manager.beginTransaction().remove(this).commit();
        manager.popBackStack();

        getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
    }*/

    /*public void onBackPressed() {
        Log.d("MA", "onBackPressedonBackPressedonBackPressedonBackPressedonBackPressed");
    }*/
}
