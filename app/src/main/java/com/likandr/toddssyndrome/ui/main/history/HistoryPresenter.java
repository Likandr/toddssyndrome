package com.likandr.toddssyndrome.ui.main.history;

import android.content.SharedPreferences;

import com.likandr.toddssyndrome.R;
import com.likandr.toddssyndrome.common.base.presenter.BasePresenter;
import com.likandr.toddssyndrome.common.misc.helpers.SharedPreferencesHelper;
import com.likandr.toddssyndrome.db.HistoryHelper;
import com.likandr.toddssyndrome.db.RxUtils;
import com.likandr.toddssyndrome.model.Entry;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.disposables.Disposable;
import io.realm.Realm;
import io.realm.RealmAsyncTask;
import io.realm.RealmChangeListener;

public final class HistoryPresenter extends BasePresenter<HistoryMVP.View>
        implements HistoryMVP.Presenter {

    private final static String TAG = HistoryPresenter.class.getName();

    private final SharedPreferences mSharedPreferences;
    private final HistoryHelper mHistoryHelper;
    private List<RealmAsyncTask> listTasks = new ArrayList<>();

    @Inject Realm mRealm;

    @Inject
    public HistoryPresenter(final SharedPreferences sharedPreferences, final HistoryHelper historyHelper) {
        this.mSharedPreferences = sharedPreferences;
        this.mHistoryHelper = historyHelper;
    }

    @Override public void attachView(HistoryMVP.View view) {
        super.attachView(view);
    }

    @Override public void detachView() {
        disposable.dispose();
        mRealm.removeChangeListener(realmListener);
        super.detachView();
    }

    private Flowable<List<Entry>> history = Flowable.empty();
    private Disposable disposable;
    private RealmChangeListener realmListener;

    @Override public void providerHistory() {
        view.showLoading();
        getHistory();//If there was a download from the server in db, this line was replaced by the request to the server

        realmListener = o -> getHistory();
        mRealm.addChangeListener(realmListener);
    }

    private void getHistory() {
        checkViewAttached();

        disposable = RxUtils.performDisposableRequest(
                mHistoryHelper.getHistory(),
                this::onGetHistorySuccess,
                this::onGetHistoryFailure);

    }

    @Override public void onGetHistorySuccess(List<Entry> entries) {
        checkViewAttached();
        view.hideLoading();
        if (entries.isEmpty()) {
            if (!SharedPreferencesHelper.getBoolean(mSharedPreferences,
                    SharedPreferencesHelper.KEY_USER_TASK_AT_LEAST_ONCE)) {
                view.showHistoryEmptyFirstTime();
            } else {
                view.showHistoryEmpty();
            }
        } else {
            SharedPreferencesHelper.putBoolean(mSharedPreferences,
                    SharedPreferencesHelper.KEY_USER_TASK_AT_LEAST_ONCE, true);
            view.showHistory(entries);
        }
    }

    @Override public void onGetHistoryFailure(Throwable e) {
        e.printStackTrace();
        checkViewAttached();
        view.hideLoading();
        view.showHistoryError();
    }

    @Override public void clearHistory() {
        RealmAsyncTask tr = mHistoryHelper.clear(
                this::clearCallBack,
                this::clearCallBack);
        listTasks.add(tr);
    }

    private void clearCallBack() {
        view.showMessage(view.getContext().getString(R.string.history_clear_success));
    }

    private void clearCallBack(Throwable throwable) {
        view.showMessage(view.getContext().getString(R.string.history_clear_error));
    }

    @Override public void closeRealm() {
        for (RealmAsyncTask item : listTasks)
            if (!item.isCancelled()) item.cancel();
        mHistoryHelper.closeRealm();
    }
}