package com.likandr.toddssyndrome.ui.main._di;

import com.likandr.toddssyndrome.common._di.AppComponent;
import com.likandr.toddssyndrome.db.HistoryHelper;
import com.likandr.toddssyndrome.ui.ActivityScope;
import com.likandr.toddssyndrome.ui.main.history.view.HistoryFragment;
import com.likandr.toddssyndrome.ui.main.history.HistoryPresenter;
import com.likandr.toddssyndrome.ui.main.slides.SlidePresenter;
import com.likandr.toddssyndrome.ui.main.slides.view.SlideFragment;

import dagger.Component;

@ActivityScope
@Component(
        dependencies = AppComponent.class,
        modules = {MainModule.class}
)
public interface MainComponent {

    //Fragments
    void inject(HistoryFragment view);
    void inject(SlideFragment view);

    // Presenters
    void inject(HistoryPresenter presenter);
    void inject(SlidePresenter presenter);

    // UseCases/Interactors
}
