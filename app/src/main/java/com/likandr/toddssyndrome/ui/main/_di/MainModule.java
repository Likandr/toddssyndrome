package com.likandr.toddssyndrome.ui.main._di;

import android.support.v4.app.Fragment;

import com.likandr.toddssyndrome.common.App;
import com.likandr.toddssyndrome.db.HistoryHelper;

import dagger.Module;
import dagger.Provides;
import io.realm.Realm;

@Module
public class MainModule {
    private Fragment fragment;

    public MainModule(Fragment fragment) {
        this.fragment = fragment;
    }

    @Provides
    Fragment provideFragment() {
        return fragment;
    }

    @Provides
    Realm provideRealm() {
        return Realm.getDefaultInstance();
    }

    @Provides
    HistoryHelper provideHistoryHelper(final Realm realm) {
        return new HistoryHelper(realm);
    }
}