package com.likandr.toddssyndrome.ui.main.slides.view;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.util.Pair;

import com.likandr.toddssyndrome.R;
import com.likandr.toddssyndrome.model.Entry;
import com.likandr.toddssyndrome.model.Question;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;

public class SlidePagerAdapter extends FragmentPagerAdapter {

    private Fragment mContext;

    static public final String TAG = SlidePagerAdapter.class.getSimpleName();
    static private int PAGE_COUNT = 5;

    public List<Question> questions;

    private Disposable mFinallyDisposable;
    private Disposable mStepDisposable;

    public boolean[] ANSVERS = new boolean[]{false, false, false, false};
    public List<Double> weights = new ArrayList<>();

    public SlidePagerAdapter(Fragment context, FragmentManager fm) {
        super(fm);
        this.mContext = context;

        mFinallyDisposable = SlidePageFragment.finallySubject.firstOrError().subscribe(this::saveNewEntry);
        mStepDisposable = SlidePageFragment.stepSubject.subscribe(this::saveAndGoNextStep);
    }

    public void onDestroy() {
        mFinallyDisposable.dispose();
        mStepDisposable.dispose();
    }

    @Override
    public Fragment getItem(int position) {
        if (position == PAGE_COUNT - 1) {
            return SlidePageFragment.newInstance(true, position);
        } else {
            return SlidePageFragment.newInstance(questions.get(position).getText(), false, position);
        }
    }

    //save new Entry
    private void saveNewEntry(@NonNull Entry entry) {
        ((SlideFragment) mContext).saveNewEntry(entry);
    }

    //save ask on question on page and go next page
    private void saveAndGoNextStep(@NonNull Pair<Boolean, Integer> pair) {
        ANSVERS[pair.second] = pair.first;
        ((SlideFragment) mContext).goNextPage();
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public int getItemPosition(Object object) {
        if (object instanceof SlidePageFragment) {
            ((SlidePageFragment)object).update(getResult());
        }
        return super.getItemPosition(object);
    }

    private String getResult() {
        float Sum = 0;
        float SumMax = 0;

        for (int i = 0; i < weights.size(); i++) {
            SumMax += weights.get(i);

            if (ANSVERS[i])
                Sum += weights.get(i);
        }
        int result = (int) (((float) 100 / SumMax) * Sum);

        String resultText = result > 75 ?
                mContext.getResources().getText(R.string.high_risk).toString() : result > 25 ?
                mContext.getResources().getText(R.string.average_risk).toString() :
                mContext.getResources().getText(R.string.low_risk).toString();

        resultText += "(" + result +"%)";

        return resultText;
    }
}
