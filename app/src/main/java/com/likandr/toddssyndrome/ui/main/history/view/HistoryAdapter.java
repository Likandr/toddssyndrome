package com.likandr.toddssyndrome.ui.main.history.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.likandr.toddssyndrome.R;
import com.likandr.toddssyndrome.model.Entry;

import java.util.Collections;
import java.util.List;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> {

    private List<Entry> mEntries = Collections.emptyList();

    private final LayoutInflater inflater;
    @Nullable private OnClickListener mOnClickListener;

    public HistoryAdapter(Context context, List<Entry> entries) {
        this.mEntries = entries;
        inflater = LayoutInflater.from(context);
    }

    public void setItems(List<Entry> entries) {
        this.mEntries = entries;
        notifyDataSetChanged();
    }
    public void setOnClickListener(@Nullable OnClickListener onClickListener) {
        this.mOnClickListener = onClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        HistoryItem view = (HistoryItem) inflater.inflate(R.layout.history_item, parent, false);
        return new ViewHolder(view);
    }

    @Override public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bindTo(mEntries.get(position));
        holder.itemView.setOnClickListener(v -> {
            if (mOnClickListener != null) {
                mOnClickListener.onItemClicked(mEntries.get(position));
            }
        });
    }

    @Override public int getItemCount() {
        return mEntries.size();
    }

    public static final class ViewHolder extends RecyclerView.ViewHolder {
        private ViewHolder(HistoryItem itemView) {
            super(itemView);
        }

        private void bindTo(Entry entry) {
            ((HistoryItem) itemView).bindTo(entry);
        }
    }

    public interface OnClickListener {
        void onItemClicked(Entry entry);
    }
}
