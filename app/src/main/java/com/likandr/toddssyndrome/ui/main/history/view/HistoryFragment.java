package com.likandr.toddssyndrome.ui.main.history.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewStub;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.cremy.greenrobotutils.library.ui.SnackBarUtils;
import com.likandr.toddssyndrome.R;
import com.likandr.toddssyndrome.common.App;
import com.likandr.toddssyndrome.common.base.BaseActivity;
import com.likandr.toddssyndrome.common.base.BaseFragment;
import com.likandr.toddssyndrome.common.base.Layout;
import com.likandr.toddssyndrome.model.Entry;
import com.likandr.toddssyndrome.ui.main._di.DaggerMainComponent;
import com.likandr.toddssyndrome.ui.main._di.MainComponent;
import com.likandr.toddssyndrome.ui.main._di.MainModule;
import com.likandr.toddssyndrome.ui.main.history.HistoryMVP;
import com.likandr.toddssyndrome.ui.main.history.HistoryPresenter;
import com.likandr.toddssyndrome.ui.main.slides.view.SlideFragment;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

@Layout(id = R.layout.fragment_history)
public class HistoryFragment extends BaseFragment implements HistoryMVP.View,
        Toolbar.OnMenuItemClickListener, HistoryAdapter.OnClickListener {

    public static final String TAG = HistoryFragment.class.getSimpleName();

    @BindView(R.id.root_view) CoordinatorLayout rootView;
    @BindView(R.id.recycler_view) RecyclerView mRecyclerView;
    @BindView(R.id.progress_bar) ProgressBar progressBar;
    @BindView(R.id.cta_create) FloatingActionButton ctaCreate;
    @Nullable @BindView(R.id.container_placeholder) FrameLayout containerPlaceholder;

    HistoryAdapter mAdapter;
    MenuItem mClearMenuItem;

    OnChangeFragment onChangeFragment;

    MainComponent component;
    @Inject HistoryPresenter presenter;

    @OnClick(R.id.cta_create)
    public void clickCtaCreateTask() {
        onChangeFragment.onChangeFragment(SlideFragment.TAG, null);
    }

    @Override public void injectDependencies() {
        if (component == null) {
            component = DaggerMainComponent.builder()
                    .appComponent(App.get().getComponent())
                    .mainModule(new MainModule(this))
                    .build();
            component.inject(this);
        }
    }

    @Override public void attachToPresenter() {
        this.presenter.attachView(this);
    }

    @Override public void detachFromPresenter() {
        Log.d(TAG, "detachFromPresenter");
        this.presenter.detachView();
    }

    public HistoryFragment() {
        // Required empty public constructor
    }

    public static HistoryFragment newInstance() {
        return new HistoryFragment();
    }

    @Override public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        this.getArgs(savedInstanceState);
        this.setUpToolbar();
    }

    @Override public void setUpToolbar() {
        setHasOptionsMenu(true);

        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        BaseActivity activity = (BaseActivity) getActivity();
        activity.setSupportActionBar(toolbar);

        toolbar.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.action_clear_history:
                    presenter.clearHistory();
                    return true;
            }
            return false;
        });

        ActionBar actionBar = activity.getSupportActionBar();
        if(actionBar!= null) {
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setTitle("Saved results");
        }
    }

    @Override public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
        super.onCreateOptionsMenu(menu, inflater);

        onSetupMenu(menu);
    }

    private void onSetupMenu(Menu menu) {
        mClearMenuItem = menu.findItem(R.id.action_clear_history);
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onViewCreated");
        super.onViewCreated(view, savedInstanceState);
        this.initRecyclerView();
        presenter.providerHistory();
    }

    @Override public boolean onMenuItemClick(MenuItem item) {
        return false;
    }

    @Override public void onAttach(Context context) {
        Log.d(TAG, "onAttach");
        this.injectDependencies();
        this.attachToPresenter();
        super.onAttach(context);
        if (context instanceof OnChangeFragment) {
            onChangeFragment = (OnChangeFragment) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnChangeFragment");
        }
    }

    @Override public void onDetach() {
        Log.d(TAG, "onDetach");
        detachFromPresenter();
        super.onDetach();
    }

    @Override public void onDestroyView() {
        Log.d(TAG, "onDestroyView");
        presenter.closeRealm();
        super.onDestroyView();
    }

    @Override public void getArgs(Bundle _bundle) {
        // Empty intentionally.
    }

    /*@Override public void onLandscape() {
        // Empty intentionally.
    }

    @Override public void onPortrait() {
        // Empty intentionally.
    }*/

    @Override public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override public void hideLoading() {
        progressBar.setVisibility(View.GONE);
    }

    @Override public void showMessage(String message) {
        SnackBarUtils.showSimpleSnackbar(rootView, message);
    }

    /*@Override public void showNoNetwork() {
        // Empty intentionally.
    }*/

    @Override public void showHistoryEmptyFirstTime() {
        setViewStub(true);

        if (containerPlaceholder != null) {
            final TextView tvPlaceholder = (TextView) containerPlaceholder.findViewById(R.id.tv_placeholder);
            tvPlaceholder.setText(getString(R.string.placeholder_empty_history_first));
        }
    }

    @Override public void showHistory(List<Entry> entries) {
        setViewStub(false);

        if (mAdapter != null) {
            mAdapter.setItems(entries);
        } else {
            mAdapter = new HistoryAdapter(getFragmentActivity(), entries);
            mAdapter.setOnClickListener(this);
        }
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override public void showHistoryEmpty() {
        setViewStub(true);

        if (containerPlaceholder != null) {
            final TextView tvPlaceholder = (TextView) containerPlaceholder.findViewById(R.id.tv_placeholder);
            tvPlaceholder.setText(getString(R.string.placeholder_empty_history));
        }
    }

    @Override public void showHistoryError() {
        //mb will be error pic for notice
    }

    @Override public void onItemClicked(Entry entry) {
        SnackBarUtils.showSimpleSnackbar(rootView, entry.getResult() + " " + entry.getDate());
    }

    public void initRecyclerView() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setAdapter(mAdapter);
    }

    private void setViewStub(boolean b) {
        if (b) {
            if (mClearMenuItem != null) mClearMenuItem.setVisible(false);

            mRecyclerView.setVisibility(View.GONE);
            if (containerPlaceholder == null && getView() != null) {
                containerPlaceholder = (FrameLayout) ((ViewStub) getView().findViewById(R.id.viewstub_history)).inflate();
            }
            containerPlaceholder.setVisibility(View.VISIBLE);
        } else {
            if (mClearMenuItem != null) mClearMenuItem.setVisible(true);

            mRecyclerView.setVisibility(View.VISIBLE);
            if (containerPlaceholder != null) {
                containerPlaceholder.setVisibility(View.GONE);
            }
        }
    }
}
