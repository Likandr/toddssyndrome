package com.likandr.toddssyndrome.ui.main.slides;

import android.support.annotation.NonNull;

import com.likandr.toddssyndrome.api.mock.SeedManager;
import com.likandr.toddssyndrome.api.serializers.AppGson;
import com.likandr.toddssyndrome.common.base.presenter.BasePresenter;
import com.likandr.toddssyndrome.db.HistoryHelper;
import com.likandr.toddssyndrome.model.Entry;
import com.likandr.toddssyndrome.model.Question;
import com.likandr.toddssyndrome.model.QuestionData;
import com.likandr.toddssyndrome.ui.main.slides.view.SlidePagerAdapter;

import javax.inject.Inject;

public class SlidePresenter extends BasePresenter<SlideMVP.View>
        implements SlideMVP.Presenter {

    private static final String TAG = SlidePresenter.class.getName();
    private static final String QUESTIONS = "questions";

    private final HistoryHelper mHistoryHelper;
    private SlidePagerAdapter pagerAdapter;

    @Inject
    public SlidePresenter(final HistoryHelper historyHelper) {
        this.mHistoryHelper = historyHelper;
    }

    public void setAdapter(SlidePagerAdapter pagerAdapter){
        this.pagerAdapter = pagerAdapter;
    }

    @Override
    public void attachView(SlideMVP.View view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    @Override
    public void start() {
        String json = new SeedManager().getJson(QUESTIONS);
        QuestionData questionRealm = AppGson.getInstance().fromJson(json, QuestionData.class);

        pagerAdapter.questions = questionRealm.getQuestions();

        for (Question question : questionRealm.getQuestions())
            pagerAdapter.weights.add(question.getWeight());
    }

    @Override
    public void addEntry(@NonNull Entry entry) {
        mHistoryHelper.addEntry(entry);
    }

    @Override
    public void closeRealm() {
        mHistoryHelper.closeRealm();
    }
}
