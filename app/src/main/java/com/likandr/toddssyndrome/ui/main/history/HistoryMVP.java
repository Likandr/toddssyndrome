package com.likandr.toddssyndrome.ui.main.history;

import com.likandr.toddssyndrome.common.base.BaseMvpView;
import com.likandr.toddssyndrome.model.Entry;

import java.util.List;

public class HistoryMVP {

    public interface View extends BaseMvpView {
        void showHistory(List<Entry> entries);
        void showHistoryEmpty();
        void showHistoryEmptyFirstTime();
        void showHistoryError();
    }

    interface Presenter {
        void providerHistory();
        void onGetHistorySuccess(List<Entry> entries);
        void onGetHistoryFailure(Throwable e);

        void clearHistory();
        void closeRealm();
    }
}
