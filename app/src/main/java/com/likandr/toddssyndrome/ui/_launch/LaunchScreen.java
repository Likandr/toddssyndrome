package com.likandr.toddssyndrome.ui._launch;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.cremy.greenrobotutils.library.ui.ActivityUtils;
import com.likandr.toddssyndrome.ui.main.MainActivity;

public class LaunchScreen extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.next();
    }

    private void next() {
        finish();
        ActivityUtils.cancelCloseAnimation(this);
        MainActivity.startMe(this);
    }
}
