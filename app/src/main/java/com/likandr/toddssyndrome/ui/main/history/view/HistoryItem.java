package com.likandr.toddssyndrome.ui.main.history.view;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.widget.TextView;

import com.likandr.toddssyndrome.R;
import com.likandr.toddssyndrome.model.Entry;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HistoryItem extends CardView {

    @BindView(R.id.history_title) TextView title;
    @BindView(R.id.history_date) TextView date;

    public HistoryItem(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
    }

    public void bindTo(final Entry item) {
        title.setText(item.getResult());
        date.setText(item.getDate());
    }
}
