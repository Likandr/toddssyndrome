package com.likandr.toddssyndrome.ui.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.util.Pair;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.likandr.toddssyndrome.R;
import com.likandr.toddssyndrome.common.base.BaseActivity;
import com.likandr.toddssyndrome.common.base.BaseFragment;
import com.likandr.toddssyndrome.common.base.Layout;
import com.likandr.toddssyndrome.ui.main.history.view.HistoryFragment;
import com.likandr.toddssyndrome.ui.main.slides.view.SlideFragment;

@Layout(id = R.layout.activity_main)
public class MainActivity extends BaseActivity implements BaseFragment.OnChangeFragment {

    private String currentFragmentTag;

    public static void startMe(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.getExtras(getIntent());
        this.setUpToolbar();

        initFragment(HistoryFragment.TAG);
    }

    private void initFragment(String fragmentTag) {
        currentFragmentTag = fragmentTag;
        Fragment fragment = getFragment(fragmentTag);

        FragmentTransaction fragmentTransaction = getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame, getFragment(fragmentTag));

        fragmentTransaction.replace(R.id.content_frame, fragment).commit();
    }

    private Fragment getFragment(String fragmentTag) {

        if (fragmentTag.equals(SlideFragment.TAG)) {
            return SlideFragment.newInstance();
        }

        return HistoryFragment.newInstance();
    }

    @Override
    public void getExtras(Intent _intent) {

    }

    @Override
    public void closeActivity() {
        this.finish();
    }

    @Override
    public void setUpToolbar() {
        setTitle(null);

        Toolbar topToolBar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(topToolBar);
    }

    @Override
    public Fragment getAttachedFragment(int id) {
        return getSupportFragmentManager().findFragmentById(id);
    }

    @Override
    public Fragment getAttachedFragment(String tag) {
        return getSupportFragmentManager().findFragmentByTag(tag);
    }

    @Override
    public void onChangeFragment(String fragmentTag,
                                 @Nullable Pair<View, String> sharedElement) {
        initFragment(fragmentTag);
    }

    @Override
    public void onBackPressed() {
        if (!currentFragmentTag.equals(HistoryFragment.TAG)) {
            initFragment(HistoryFragment.TAG);
        } else {
            finish();
        }
    }
}
