package com.likandr.toddssyndrome.ui.main.slides.view;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.likandr.toddssyndrome.R;
import com.likandr.toddssyndrome.common.misc.widgets.MaterialDesignFlatButton;
import com.likandr.toddssyndrome.model.Entry;

import java.util.Calendar;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.subjects.PublishSubject;

public class SlidePageFragment extends Fragment implements SlideFragment.Updateable {

    public static final String TAG = SlidePageFragment.class.getSimpleName();

    static final String ARGUMENT_QUESTION = "arg_question";
    static final String ARGUMENT_IS_LAST_PAGE = "arg_is_last_page";
    static final String ARGUMENT_POSITIONS = "arg_position";

    @BindView(R.id.tvPage) TextView tvPage;
    @BindView(R.id.btnYes) MaterialDesignFlatButton btnYes;
    @BindView(R.id.btnNo)  MaterialDesignFlatButton btnNo;

    String question = "";
    boolean isLastPage;
    int position;
    int backColor;

    public static PublishSubject<Pair<Boolean, Integer>> stepSubject = PublishSubject.create();
    public static PublishSubject<Entry> finallySubject = PublishSubject.create();

    //for last page
    public static SlidePageFragment newInstance(boolean isLastPage, int position) {
        SlidePageFragment pageFragment = new SlidePageFragment();
        Bundle arguments = new Bundle();
        arguments.putBoolean(ARGUMENT_IS_LAST_PAGE, isLastPage);
        arguments.putInt(ARGUMENT_POSITIONS, position);
        pageFragment.setArguments(arguments);
        return pageFragment;
    }

    //for non last pages
    public static SlidePageFragment newInstance(String str, boolean isLastPage, int position) {
        SlidePageFragment pageFragment = new SlidePageFragment();
        Bundle arguments = new Bundle();
        arguments.putString(ARGUMENT_QUESTION, str);
        arguments.putBoolean(ARGUMENT_IS_LAST_PAGE, isLastPage);
        arguments.putInt(ARGUMENT_POSITIONS, position);
        pageFragment.setArguments(arguments);
        return pageFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        question = getArguments().getString(ARGUMENT_QUESTION);
        isLastPage = getArguments().getBoolean(ARGUMENT_IS_LAST_PAGE);
        position = getArguments().getInt(ARGUMENT_POSITIONS);

        Random rnd = new Random();
        backColor = Color.argb(40, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_slide_page, null);
        ButterKnife.bind(this, view);

        tvPage.setText(question);
        tvPage.setBackgroundColor(backColor);

        if (isLastPage) {
            btnYes.setText(getResources().getText(R.string.save_and_back).toString());
            btnNo.setVisibility(View.GONE);
        } else {
            btnYes.setText(getResources().getText(R.string.btnYes).toString());
            btnNo.setText(getResources().getText(R.string.btnNo).toString());
        }

        return view;
    }

    @OnClick(R.id.btnYes) public void onClickYes() {
        Entry entry = new Entry(tvPage.getText().toString(), getCurrentDate());

        if (isLastPage) {
            finallySubject.onNext(entry);
            backToHistory();
        } else {
            stepSubject.onNext(new Pair<>(true, position));
        }
    }

    @OnClick(R.id.btnNo) public void onClickNo() {
        stepSubject.onNext(new Pair<>(false, position));
    }

    @Override
    public void update(String str) {
        tvPage.setText(str);
    }

    private String getCurrentDate() {
        Calendar rightNow = Calendar.getInstance();
        long offset = rightNow.get(Calendar.ZONE_OFFSET) +  rightNow.get(Calendar.DST_OFFSET);

        return Long.toString((rightNow.getTimeInMillis() + offset) %  (24 * 60 * 60 * 1000));
    }

    private void backToHistory() {
        getActivity().onBackPressed();
    }
}
