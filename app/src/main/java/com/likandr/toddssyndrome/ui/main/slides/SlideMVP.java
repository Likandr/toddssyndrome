package com.likandr.toddssyndrome.ui.main.slides;

import android.support.annotation.NonNull;

import com.likandr.toddssyndrome.common.base.BaseMvpView;
import com.likandr.toddssyndrome.model.Entry;

public class SlideMVP {

    public interface View extends BaseMvpView {
        void goNextPage();
        void saveNewEntry(@NonNull Entry entry);
    }

    interface Presenter {
        void start();
        void addEntry(@NonNull Entry entry);
        void closeRealm();
    }
}
