package com.likandr.toddssyndrome.db;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class RxUtils {

    public static <T> Disposable performDisposableRequest(Flowable<T> flowable, Consumer<? super T> consumer, Consumer<Throwable> onError) {
        return flowable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(consumer, onError);
    }
}
