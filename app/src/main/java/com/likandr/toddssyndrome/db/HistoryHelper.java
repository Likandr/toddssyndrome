package com.likandr.toddssyndrome.db;

import android.support.annotation.NonNull;

import com.likandr.toddssyndrome.model.Entry;

import java.util.List;

import io.reactivex.Flowable;
import io.realm.Realm;
import io.realm.RealmAsyncTask;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

public class HistoryHelper {

    public static final String TAG = HistoryHelper.class.getSimpleName();

    private final Realm mRealm;

    public HistoryHelper(final Realm realm) {
        mRealm = realm;
    }

    public void closeRealm() {
        mRealm.close();
    }

    public void addEntry(@NonNull Entry entry) {
        try  {
            mRealm.executeTransactionAsync(realm1 -> realm1.copyToRealmOrUpdate(entry));
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public RealmAsyncTask clear(Realm.Transaction.OnSuccess onSuccess,
                                Realm.Transaction.OnError onError) {
            return mRealm.executeTransactionAsync(realm1 -> realm1.delete(Entry.class),
                    onSuccess, onError);
    }

    public Flowable<List<Entry>> getHistory() {
        RealmResults<Entry> results = mRealm.where(Entry.class).findAllAsync();
        return Flowable.just(mRealm.copyFromRealm(results));
    }
}