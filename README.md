# README #

The application by passing the test gives the probability of the presence of the disease.

### The application consists of two screens ###
1. saved results, previously passed tests. On the "+" you can start the test.
2. Test (questions are replaced by slides)

### Used libraries ###
 * REALM 			(3.4.0)
 * RETROFIT 		(2.0.2)
 * OKHTTP 			(3.6.0)
 * LEAKCANARY 		(1.5)
 * DAGGER_VERSION 	(2.9)
 * GSON_VERSION 	(2.6.2)
 * BUTTERKNIFE 		(8.5.1)
 * RXJAVA 			(2.0.8)
 * RXANDROID 		(2.0.1)